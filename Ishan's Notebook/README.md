9/14/2021 - 9/16/2021: Worked with Danielle on Project Proposal for an hour. I worked on the introduction and conclusion and did further research on guitar specific terminology. Also worked on soldering assignment in the lab.

9/23/2021 - 9/25/2021: Spoke with Danielle about the fundamentals of our project and she explained the top level block diagram. Gained a much better understanding of the data flow. Did more research about guitar specific technical knowledge such as inputs, midi, and 1/4 inch jacks.

9/27/2021 - 9/30/2021: Worked on Design Document with team. I did block diagram and did research on resistor value for tolerance analysis. Learned a lot about how to decide proper resitance for op amp adder circuit.

9/31/2021 - 10/3/2021: Did more research on how to start building the device. Used this website: 
https://learn.sparkfun.com/tutorials/proto-pedal-example-programmable-digital-pedal/all#assembly-part-2-teensy-and-controls
which had a lot of useful definitions and procedures. Made the circuit schematic for op amp adder and sent to danielle so she could add it to overall schematic.

10/4/2021 - 10/6/2021: Prepared my parts for the design review and practiced my part of the presentation which was the intro, objectives, and possible solutions. Did futher research to be more knowledgeable about the harmonies and waveforms that we would allow the user to select. Used a helpful link to assist with research: https://www.electronics-tutorials.ws/opamp/opamp_4.html

10/10/2021 - 10/13/2021: Started to design PCB board after Danielle sent me the overall circuit schematic on EAGLE. Had to download and learn how to use the software first which took most of a day. Ordered majority of parts after researching quantity and type of parts needed. 

10/20/2021 - 10/25/2020: Could not finish PCB design in time for first Pass. Tried to relayer PCB and get it to work, Danielle was able to get it working. Began researching on ADC/DAC subsystem but Madhav told me he wanted to implement in software so I switched learning about MIDI Note Generation.

11/1/2021 - 11/4/2021: Worked on Individual Progress Report and worked in Lab with the team. Looked up any questions we had about pass through function on my laptop while Madhav implemented it in Arduino.

11/08/2021 - 11/11/2021: Went to lab to work and picked up parts from target on the way. Picked up an aux cable and two way guitar cable that we thought we had but did not. Did research on YIN algorithm to gain a better understanding. This link was very helpful in studying the documentation: http://mroy.chez-alice.fr/yin/index.html#:~:text=The%20Yin%20algorithm%20was%20developed,rate%20and%20few%20tuning%20parameters.

11/14/2021 - 11/17/2021: We found an error in our PCB and realized we needed to redesign some parts. Danielle explained how the new pcb design matched up with the schematic. Ordered the new PCB.

11/17/2021 - 11/19/2021: Prepared for mock demo. Went to the guitar store to pickup an extra patch cable and guitar aux cable because we needed new ones. Finished Mock Demo. Midi part needed for subsystem delayed in the mail so not able to continue with that yet.

11/29/2021 - 12/1/2021: Arrived back to campus a couple hours before demo, had to stay home late for emergency reasons. Completed demo. Was unable to implement Midi subsystem after DSP because the part did not come in time. The Teensy chip overheated during the demo probably due to a loose wire or soldering short circuit. We showed videos of our working project in the demo.

12/2/2021 - 12/4/2021: Worked on mock presentation and practiced my parts to present which was introduction and objectives.

12/5/2021 - 12/7/2021: Worked on final presentation and practiced my parts to present which was lessons learned and conclusion/results and future plans for the project. Completed Final Presentation.

12/7/2021 - 12/9/2021: Worked on final paper by myself and completed all required sections. Completed and submitted final paper, lab notebook, and teamwork eval 2.









