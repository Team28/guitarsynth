

#define GUITAR_NOTE_COUNT 49
#define FREQUENCY_MULTIPLIER 0.9666

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>


AudioInputAnalog         adc1;           //xy=291,153
AudioSynthWaveform       waveform4;      //xy=298,573
AudioSynthWaveform       waveform3;      //xy=304,524
AudioSynthWaveform       waveform2;      //xy=305,470
AudioSynthWaveform       waveform1;      //xy=306,422
AudioAnalyzePeak         peak1;          //xy=538,517
AudioMixer4              mixer1;         //xy=579,381
AudioAnalyzeNoteFrequency notefreq1;      //xy=627,155
AudioMixer4              mixer2;         //xy=774,253
AudioOutputPT8211        pt8211_1;       //xy=955,293
AudioConnection          patchCord1(adc1, notefreq1);
AudioConnection          patchCord2(adc1, 0, mixer1, 0);
AudioConnection          patchCord3(adc1, peak1);
AudioConnection          patchCord4(waveform4, 0, mixer2, 1);
AudioConnection          patchCord5(waveform3, 0, mixer1, 3);
AudioConnection          patchCord6(waveform2, 0, mixer1, 2);
AudioConnection          patchCord7(waveform1, 0, mixer1, 1);
AudioConnection          patchCord8(mixer1, 0, mixer2, 0);
AudioConnection          patchCord9(mixer2, 0, pt8211_1, 0);

void setup() {
  // put your setup code here, to run once:
  AudioMemory(100);
  notefreq1.begin(0.15);

  waveform1.begin(WAVEFORM_SINE);
  waveform1.amplitude(0.1);
  waveform1.frequency(50);
  waveform1.pulseWidth(0.15);

  waveform2.begin(WAVEFORM_SINE);
  waveform2.amplitude(0.1);
  waveform2.frequency(50);
  waveform2.pulseWidth(0.15);

  waveform3.begin(WAVEFORM_SINE);
  waveform3.amplitude(0.1);
  waveform3.frequency(50);
  waveform3.pulseWidth(0.15);

  
  waveform4.begin(WAVEFORM_SINE);
  waveform4.amplitude(0.1);
  waveform4.frequency(50);
  waveform4.pulseWidth(0.15);

  mixer1.gain(0, 5);
  pinMode(2, INPUT); 
  pinMode(4, INPUT);
  pinMode(6, INPUT);
  pinMode(8, INPUT); 
  pinMode(10, INPUT);   
  

}

void loop() {
  // put your main code here, to run repeatedly:
float guitar_notes[GUITAR_NOTE_COUNT] = {82.41, 87.31, 92.50, 98.00, 103.8, 110.0, 116.5, 123.5,
 130.8, 138.6, 146.8, 155.6, 164.8, 174.6, 185.0, 196.0, 207.7, 220.0, 233.1, 246.9,
  261.6, 277.2, 293.7, 311.1, 329.6, 349.2, 370.0, 392.0, 415.3, 440.0, 466.2, 493.9,
 523.3, 554.4, 587.3, 622.3, 659.3, 698.5, 740.0, 784.0, 830.6, 880.0, 932.3, 987.8,
  1047,  1109,  1175,  1245 , 1319}; 

   int wave_style_continuous = digitalRead(2);
   int major_minor = digitalRead(4);
   int octave_on = digitalRead(6);
   int third_on = digitalRead(8);
   int fifth_on = digitalRead(10);

//   if(wave_style){}

  
  if (peak1.available())
{
    float peakv = peak1.read();

    waveform1.amplitude(sqrt(10000*peakv));
    waveform2.amplitude(sqrt(10000*peakv));
    waveform3.amplitude(sqrt(10000*peakv));
   
 

       if (notefreq1.available())
{

        
        float target_freq = notefreq1.read();

        float prob = notefreq1.probability();

       float note = bin_search(guitar_notes, 0, 48, target_freq * FREQUENCY_MULTIPLIER);

        if (!wave_style_continuous){
          if (target_freq < 80){target_freq = 82;}
          if (target_freq > 1400){target_freq = 1350;}
          }
        else{note = target_freq;}
       

        
        if(octave_on){waveform1.frequency(note * 0.5);}
        else{waveform1.frequency(0);}
        
        if(fifth_on){waveform2.frequency(note * 1.49831);}
        else{waveform2.frequency(0);}
     
        if(third_on){
          if(!major_minor){
            waveform3.frequency(note * 1.18921); 
            waveform4.frequency(note * 1.782); 
            } // minor
          else{
            waveform3.frequency(note * 1.25992);
            waveform4.frequency(note * 1.888);
            } //major
          }
         else{waveform3.frequency(0);}
 
        
        Serial.printf("Frequency:%3.2f | Note:%3.2f | Probability: %.2f | PeakIn: %.5f\n",target_freq, note, prob, peakv);
     } 

     

   }

  
}


float bin_search(float *arr, int start_idx, int end_idx, float search_val) {

   if( start_idx == end_idx )
      return arr[start_idx] <= search_val ? arr[start_idx] : arr[start_idx];

   int mid_idx = start_idx + (end_idx - start_idx) / 2;

   if( search_val < arr[mid_idx] )
      return bin_search( arr, start_idx, mid_idx, search_val );

   float ret = bin_search( arr, mid_idx+1, end_idx, search_val );
   return ret == 0 ? mid_idx : ret;
}
