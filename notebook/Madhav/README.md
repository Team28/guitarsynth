# Madhav Worklog

## 09/10/2021
I worked on the initial idea post, which was something I decided on after discussing that the previous idea of a YOLOv3 AI glasses was not feasible. As a guitarist myself, I tried to imagine a product that would help me the most in a live or studio setting. After discussing with other musicians I know, I settled on the idea for a harmony synthesizer guitar pedal.

## 09/13/2021
We worked on the project proposal. Danielle is not a guitarist, so we got familiar with the basics of an electric guitar's signal chain and common effects that guitarists use.

## 09/15/2021
Worked on the project proposal, detailing the ADC/DAC requirements and the DSP requirements that I thought would be most relevant to signal processing with line-level inputs.

## 09/16/2021
Worked on and submitted the project proposal today. Also did research into what platforms/microcontrollers/DSP libraries we could use. Ref: 
- Daisy, "Daisy Seed" 2021, https://www.google.com/search?q=daisy+seed&oq=daisy+see&aqs=chrome.1.69i57j69i59j35i39j0i512l4j69i60.2137j0j1&sourceid=chrome&ie=UTF-8  
- PJRC Teensy 3.2, "Teensy 3.2" 2021, https://www.pjrc.com/store/teensy32.html 
- PJRC Teensy Audio Shield "Teensy Audio Shield" 2021, https://www.pjrc.com/store/teensy3_audio.html 


## 09/24/2021
- Worked on the Design Document, expanded on the design requirements that would be required for maximised compatibility with a guitarist's existing pedal chain. 
- This included things like the power supply, input/output settings as well as tolerances with regard to latency and temperature. Ref: Pete's DIY Effects, 2021, https://diyeffectspedals.com/common-diy-guitar-effects-pedal-components/

## 09/27/2021
I worked on the circuit schematic for the PCB, as well as researched the materials/dimensions that are seen on best-selling guitar pedals. Ref: Pete's DIY Effects, 2021, https://diyeffectspedals.com/common-diy-guitar-effects-pedal-components/ 
![firstschematic](notebook/Madhav/SCH.png "first schematic for project")


## 09/28/2021
We delegated parts between ourselves in order to focus on particular subsystems. I took on DSP and ADC/DAC. To further look into how we would implement frequency estimation as well as quick resolution of the input to the closest notes, I looked into the music-theoretical standards of how the tuning/temperament for 'correct' notes should be programmed. Danielle and I discussed the layout of the schematic.
![Schedule](notebook/Madhav/schedual.PNG "Schedule for project")

## 09/29/2021
Worked on the Design document (6 hours) and discussed our schedule.
## 09/30/2021
Worked on the Design Document.
## 09/31/2021
Submitted final Design Document!

## 10/4/2021
We had to reshuffle our parts today, with me also taking on the physical design/placement of switches and ports. Decided on some different kinds of switches Ref: Arrow, Understand the Fundamentals of Switches, https://www.arrow.com/en/research-and-events/articles/understand-the-fundamentals-of-switch-poles-and-throws#:~:text=What%20is%20a%20DPDT%20Switch,four%20output%20terminals%20are%20separate.


## 10/5/2021
Practised for the design review with the team, made sure we had thought through all aspects of our design and placed orders for the Teensy/Audio Shield for myself as well. - PJRC Teensy Audio Shield "Teensy Audio Shield" 2021, https://www.pjrc.com/store/teensy3_audio.html 

## 10/6/2021
We had the DR today and I think we were able to justify our design decisions well.

## 10/13/2021 
Danielle had started the first round of the PCB designs and I helped by ensuring that the layout was consistent with the physical design of the guitar pedal, and that the ports would face the outer portions of the chassis. 

## 10/20/2021
PCB arrived. Ordered a set of pedal chassis from AMazon. Ref DaierTek 1590XX 1790NS Guitar Effects Pedal Enclosure, https://www.amazon.com/dp/B08K7GHTVP?psc=1&ref=ppx_yo2_dt_b_product_details

## 10/23/2021
Tried to start soldering the PCB but I realised that the LM358 parts we had were through-hole whereas the PCB was surface-mount. This meant that we had to make deep edits to the PCB design.

## 10/25/2021
I ordered another Teensy for myself, just in case we damaged the first chip.

## 10/26/2021
Spent 6 hours in lab setting up the circuit for Teensy and Audio Shield, as well as for writing a barebones program that simply throughputted the guitar signal from the Teensy. This was done as a sanity check for the latency as well as for the quality of the signal which I found to be satisfactory. 
![Barebones](notebook/Madhav/Barebones.PNG "Audio Chain")


## 10/28/2021
Spent more time in the lab getting the Teensy and Audio Shield circuit to produce waveforms. Today I was able to ensure that we could output a variety of waveforms, from sine to square to sawtooth.
![Full_Chain](notebook/Madhav/Chain.PNG "DSP Audio Chain Chain")




## 11/06/2021
Demonstrated to Danielle that the circuit worked as a proof of concept. We now had to process the signal in the analog domain for which Danielle had to build the I/O buffers and power systems.
![Bread_board](notebook/Madhav/breadboard.PNG "Breadboard circuit")

## 11/07/2021
Daniele made the I/O buffers and we tested the design on a breadboard. We are extremely pleased that the system works quite well on a breadboard. We also tested some resistor values and capacitor values.
## 11/08/2021
We started a new PCB design with through-hole components so we wouldn't have to spend more money buying surface-mount components. This was because our old PCB was very difficult to solder due to there being a lot of surface-mount pads that we didn't have components for.

## 11/10/2021
Finished the PCB design with Danielle. Also ordered a DAC chip that would be a lot cheaper than an audio shield, this is because the DAC chip will be a lot more affordable and have a smaller footprint in the pedal. The Teensy is already capable of 16-bit ADC so this was needed just for DAC.

## 11/11/2021
We talked with David about our new PCB design. I also started working on a new PCB design myself as I decided to implement the signal balancing in software, so as to ensure a level of flexibility for the guitarist.
## 11/15/2021
I ordered my own new PCB Design as well another Chassis and switches/connectors for the Teensy. 
## 11/16/2021
Finalized the software functionality for the Teensy. Implemented a modified version of binary search, stored the note values in a sorted array and added lots of complexity to the software by way of adding digital inputs for the switches amongst other things.
![Full_Chain](notebook/Madhav/Chain.PNG "DSP Audio Chain Chain")



    #define GUITAR_NOTE_COUNT 49
    #define FREQUENCY_MULTIPLIER 0.9666

    #include <Audio.h>
    #include <Wire.h>
    #include <SPI.h>
    #include <SD.h>
    #include <SerialFlash.h>


    AudioInputAnalog         adc1;           //xy=291,153
    AudioSynthWaveform       waveform4;      //xy=298,573
    AudioSynthWaveform       waveform3;      //xy=304,524
    AudioSynthWaveform       waveform2;      //xy=305,470
    AudioSynthWaveform       waveform1;      //xy=306,422
    AudioAnalyzePeak         peak1;          //xy=538,517
    AudioMixer4              mixer1;         //xy=579,381
    AudioAnalyzeNoteFrequency notefreq1;      //xy=627,155
    AudioMixer4              mixer2;         //xy=774,253
    AudioOutputPT8211        pt8211_1;       //xy=955,293
    AudioConnection          patchCord1(adc1, notefreq1);
    AudioConnection          patchCord2(adc1, 0, mixer1, 0);
    AudioConnection          patchCord3(adc1, peak1);
    AudioConnection          patchCord4(waveform4, 0, mixer2, 1);
    AudioConnection          patchCord5(waveform3, 0, mixer1, 3);
    AudioConnection          patchCord6(waveform2, 0, mixer1, 2);
    AudioConnection          patchCord7(waveform1, 0, mixer1, 1);
    AudioConnection          patchCord8(mixer1, 0, mixer2, 0);
    AudioConnection          patchCord9(mixer2, 0, pt8211_1, 0);

    void setup() {
    // put your setup code here, to run once:
    AudioMemory(100);
    notefreq1.begin(0.15);

    waveform1.begin(WAVEFORM_SINE);
    waveform1.amplitude(0.1);
    waveform1.frequency(50);
    waveform1.pulseWidth(0.15);

    waveform2.begin(WAVEFORM_SINE);
    waveform2.amplitude(0.1);
    waveform2.frequency(50);
    waveform2.pulseWidth(0.15);

    waveform3.begin(WAVEFORM_SINE);
    waveform3.amplitude(0.1);
    waveform3.frequency(50);
    waveform3.pulseWidth(0.15);

    
    waveform4.begin(WAVEFORM_SINE);
    waveform4.amplitude(0.1);
    waveform4.frequency(50);
    waveform4.pulseWidth(0.15);

    mixer1.gain(0, 5);
    pinMode(2, INPUT); 
    pinMode(4, INPUT);
    pinMode(6, INPUT);
    pinMode(8, INPUT); 
    pinMode(10, INPUT);   
    

    }

    void loop() {
    // put your main code here, to run repeatedly:
    float guitar_notes[GUITAR_NOTE_COUNT] = {82.41, 87.31, 92.50, 98.00, 103.8, 110.0, 116.5, 123.5,
    130.8, 138.6, 146.8, 155.6, 164.8, 174.6, 185.0, 196.0, 207.7, 220.0, 233.1, 246.9,
    261.6, 277.2, 293.7, 311.1, 329.6, 349.2, 370.0, 392.0, 415.3, 440.0, 466.2, 493.9,
    523.3, 554.4, 587.3, 622.3, 659.3, 698.5, 740.0, 784.0, 830.6, 880.0, 932.3, 987.8,
    1047,  1109,  1175,  1245 , 1319}; 

    int wave_style_continuous = digitalRead(2);
    int major_minor = digitalRead(4);
    int octave_on = digitalRead(6);
    int third_on = digitalRead(8);
    int fifth_on = digitalRead(10);

    //   if(wave_style){}

    
    if (peak1.available())
    {
        float peakv = peak1.read();

        waveform1.amplitude(sqrt(10000*peakv));
        waveform2.amplitude(sqrt(10000*peakv));
        waveform3.amplitude(sqrt(10000*peakv));
    
    

        if (notefreq1.available())
    {

            
            float target_freq = notefreq1.read();

            float prob = notefreq1.probability();

        float note = bin_search(guitar_notes, 0, 48, target_freq * FREQUENCY_MULTIPLIER);

            if (!wave_style_continuous){
            if (target_freq < 80){target_freq = 82;}
            if (target_freq > 1400){target_freq = 1350;}
            }
            else{note = target_freq;}
        

            
            if(octave_on){waveform1.frequency(note * 0.5);}
            else{waveform1.frequency(0);}
            
            if(fifth_on){waveform2.frequency(note * 1.49831);}
            else{waveform2.frequency(0);}
        
            if(third_on){
            if(!major_minor){
                waveform3.frequency(note * 1.18921); 
                waveform4.frequency(note * 1.782); 
                } // minor
            else{
                waveform3.frequency(note * 1.25992);
                waveform4.frequency(note * 1.888);
                } //major
            }
            else{waveform3.frequency(0);}
    
            
            Serial.printf("Frequency:%3.2f | Note:%3.2f | Probability: %.2f | PeakIn: %.5f\n",target_freq, note, prob, peakv);
        } 

        

    }

    
    }


    float bin_search(float *arr, int start_idx, int end_idx, float search_val) {

    if( start_idx == end_idx )
        return arr[start_idx] <= search_val ? arr[start_idx] : arr[start_idx];

    int mid_idx = start_idx + (end_idx - start_idx) / 2;

    if( search_val < arr[mid_idx] )
        return bin_search( arr, start_idx, mid_idx, search_val );

    float ret = bin_search( arr, mid_idx+1, end_idx, search_val );
    return ret == 0 ? mid_idx : ret;
    }

## 11/24/2021
I worked on the hardware of the pedal chassis at home with a friend who makes pedals as guidance, as well as tested the final circuit with the PCB I had ordered, and decided to use it since it had lesser noise. The product is working perfectly. 

![Chassis](notebook/Madhav/Chassis_Side.PNG "Pedal Chassis")
![Chassis_UP](notebook/Madhav/Chassis_UP.PNG "Pedal Chassis")



## 11/27/2021
I ran into some issues as it seems the Teensy I had in the chassis has overheated and died. I am replacing it with the spare Teensy I had bought. It is working perfectly again. We are taking video evidence of all modes of operation to ensure that in case something goes wrong before the demo, we have proof that our product works.

## 11/28/2021
Closing the Chassis is still left. We are going to test the Teensy in Lab when Danielle is back from break.
## 11/29/2021
We met in lab with all the components and ensured that the device was working. Again took several videos as we were nervous that the product would stop working. We connected the power supply. A few connections were a bit loose, I think the quality of wires we orderd could have been better.

## 11/30/2021
Unfortunately, a connection came loose during demo and connected the 3.3V port on the Teensy to the 5V port. This is a common cause of Teensy chips getting fried, luckily we had video demos of all our functionality with the chassis and switches all working.

## 12/08/2021
Final Paper
My parts
- Abstract
- Intro
- DSP
- ADC/DAC
- Appendix C
- Appendix A  
- Appendix B 
- References 


