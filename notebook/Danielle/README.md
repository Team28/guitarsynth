# Danielle Worklog
- [Danielle Worklog](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#danielle-worklog)
- [09/14/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-09142021)
- [09/15/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-09152021)
- [09/16/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-09162021)
- [09/23/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-09232021)
- [09/28/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-09282021)
- [09/29/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-09292021)
- [09/30/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-09302021)
- [09/31/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-09312021)
- [10/3/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-1032021)
- [10/5/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-1052021)
- [10/6/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-1062021)
- [10/08/2021-10/12/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/blob/main/notebook/Danielle/README.md#10082021-10122021)
- [10/20/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-10202021)
- [10/23/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-10232021)
- [10/24/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-10242021)
- [10/25/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-10252021)
- [10/26/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-10262021)
- [10/27/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-10272021)
- [10/28/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-10282021)
- [10/30/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-10302021)
- [10/31/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-10312021)
- [11/01/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11012021)
- [11/04/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11042021)
- [11/05/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11052021)
- [11/06/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11062021)
- [11/07/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11072021)
- [11/08/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11082021)
- [11/09/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11092021)
- [11/10/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11102021)
- [11/11/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11112021)
- [11/15/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11152021)
- [11/17/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11172021)
- [11/18/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11182021)
- [11/19/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11192021)
- [11/24/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11242021)
- [11/25/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11252021)
- [11/26/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11262021)
- [11/27/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11272021)
- [11/28/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11282021)
- [11/29/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11292021)
- [11/30/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-11302021)
- [12/01/2021-12/02/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#12012021-12022021)
- [12/03/2021-12/06/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/blob/main/notebook/Danielle/README.md#12032021-12062021)
- [12/06/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-12062021)
- [12/07/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-12072021)
- [12/08/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-12082021)
- [12/09/2021](https://gitlab.engr.illinois.edu/Team28/guitarsynth/-/edit/main/notebook/Danielle/README.md#anchor-12092021)
## 09/14/2021
I worked on the project proposal with Ishan, (est time Ishan - 45 minutes, myself - 1.5hr). My main part on this day was setting up and formatting the block diagram, Madhav did the connections and finished defining subsystems on another day. 
## 09/15/2021
I worked on the project proposal (time 1.5 hours.)
## 09/16/2021
I worked on the project proposal (time 6.5 hours.) I created a project study guide to help me understand parts of the project that I dont know as a non-guitarist. 
Main ideas I learned
- The guitar signal will be attenuated by input hardware 
- Go into teensy audio board.
- Adc will occur.
- Will find the closest Midi note.
- We will have a selector that picks an octave.
- Will either go into our internal synthisizor or external synth will be an option. There will be another slector for the type of synth we perform.
- Dac will occur.
- Signal will go through output logic on the board.
- Then the original signal will be combined with this signal.
## 09/23/2021
Created the design doc.
## 09/26/2021
Edited the project proposal to fit the design document.
## 09/28/2021
I realized we were looking at the wrong outline for the Design Review. We worked together to try to complete the most we could for our review.
Created the Final design document and split up parts that everyone would be responsible for based on estimated time of completion. 
I made a list of the feedback recieved from the first design review.
I created a pcb document of the different aspects of our project we will need on the PCB board so that our team could add research/findings to created pcb layout there.
## 09/29/2021
Formatted the Final Design Document.
## 09/30/2021
Finished my parts of the Final Design Document. (est time 8 hours)
Hardware power, input, and output Reference: Byron, "Proto Pedal Example: Programmable Digital Pedal"  2021, 
https://learn.sparkfun.com/tutorials/proto-pedal-example-programmable-digital-pedal/all#assembly-part-2-teensy-and-controls 
Proposed tests:
- input buffer: Check the input buffer divides the peak-to-peak voltage by 3. So that it fits in range of audio board.
- The input buffer: impedance is greater than 100kOhms. so that the signal is not attenuated since output impedance will be small.
- input buffer: Input is equal to or less than 1Vrms . This is because 1Vrms is a little more than the highest guitar output voltages.
Mottola, "Table of Musical Notes and Their Frequencies and Wavelengths" 2020, 
https://www.liutaiomottola.com/formulae/freqtab.htm 
The digital signal is communicated to the microcontroller. 
- ADC: should be able to convert in the frequent range of 50-1500Hz with maximum ±5% signal distortion. This is because even with ±5% signal distortion, the fundamental frequency will be close enough to a ‘correct’ note that it can be resolved to it.
- DAC: Output is equal to or less than 1Vrms . This is because 1Vrms is a little more than the highest guitar output voltages.
- DAC: should be able to convert in the frequent range of 50-3000Hz with maximum ±1% signal distortion. This is a tighter boundary, as these are the frequencies that the user will actually hear and they need to be a lot more accurate than the ones required for note computation.
This subsystem should be able to multiply the peak-to-peak voltage headroom of an input signal by approximately 3. It should be able to do so for signals between 0.5V and 5V peak-to-peak voltage.
- Output buffer: The impedance is greater than 100kOhms to prevent signal attenuation
- Output buffer:The output from the DAC and the raw guitar signal should be balanced in rms Voltage. For combination.

## 09/31/2021
The Design Document was submitted today. 
The schedual for the project I created is:
![schedual](notebook/Danielle/schedual.PNG "schedual for project")
## 10/3/2021
Update to schedule: I have taken Ishans part of figuring out the connections to the Teensy 3.2, audio sheild and input voltage regulation for the PCB board. I used Eagle to make the schematic for this part and sent it to my team. (est time 4 hours)
## 10/5/2021
Met with team and deligated what parts we are reposible for speaking about for the Design review tomorrow. I practiced my parts, created a hand drawn visual aid, and created a list of questions I expect everyone to be asked to help my team prepare. est time (3 hours)
## 10/6/2021
Today we presented our project and I worked on transfering the schematic to the PCB for our review. (est time 3 hours)
## 10/08/2021 - 10/12/2021
Reorganized pcb design into subsections and designed PCB board from scratch.(est time 12 hours)
![PCB](notebook/Danielle/PCB.png " first PCB for project")
![firstschematic](notebook/Danielle/SCH.png "first schematic for project")
## 10/20/2021
PCB order was delivered.
## 10/23/2021
Met with group in lab realized we were missing some parts from or orders including the midi connector, audio jacks, and LM358.
## 10/24/2021
Went to lab with Madhav to work on teensy connections.
## 10/25/2021
I ordered the midi connector, audio jacks, and LM358.
## 10/28/2021
Restructured block diagram with help from Madhav for corrections to our Design Document.
## 10/30/2021
Audio jacks said they arrived but package was empty. It got lost in the mail.
## 10/31/2021
Went to lab and verified power subsystem. Also built other hardware on breadboard.
Started working on the individual report. The power system gave constant value close to 5v with roughly 0.5% error.

## 11/01/2021
Finished Individual report and submitted. Took responsibility of input buffer, output buffer, power, combinational logic, ADC,DAC.
## 11/04/2021
The audio jacks arrived and i went into lab with them. Went to lab Madhav forgot cable for the guitar. We looked over software aspects.
## 11/05/2021
Bought air cables and a aux cable from the ECEb store. Met with Madhav in lab worked on verifications.
## 11/06/2021
Dropped of the guitar at the ECEB and Madhav demonstrated the Teensy outputing sound from the chords he was playing. This did not include MIDI or any hardware besides the teensy.
## 11/07/2021
Worked most of the day in the lab. I noticed from how I was connected the input buffer to power and GND across R3 and R4 was causing the voltage for the power terminal itself to drop from 9v to just a few mv. Even the input from the power input shorted when in the terminal. I fixed this by replacing wire connections to groung and power with the circuit components.
## 11/08/2021
Started a new PCB design. I relized on the old pcb that components that were meant to be on the bottom layer ended up on the top. Since I had to flip many compenents the design was backwards from the schematic so I redid the component layout. I also made soldering pads at many places since we decided earlier to do that rather than connect everything to teensy. The reason we didnt want to make the connections was fear of mistake and also since we made the new DAC board. 
## 11/09/2021
I finished routing my new PCB design, but am still left with some concerns. I also left solderpoints for the midi instead of making connections since we have yet to get midi working and tested. The connector we have for MIDI is only for the PCB so we havent been able to verify MIDI on the breadboard.
The figure shows the new PCB design.
![newPCB](notebook/Danielle/NewPCB.png "PCB for project")
![newschematic](notebook/Danielle/SCH2.png "schematic for project")
We decided to not have teensy connections and midi connection because they are not verified yet so this way we have more flexibility.
Reference: Byron, "Proto Pedal Example: Programmable Digital Pedal"  2021, 
https://learn.sparkfun.com/tutorials/proto-pedal-example-programmable-digital-pedal/all#assembly-part-2-teensy-and-controls 
10/32=1+10k/(22k+10k) This is the input buffers attenuation equation based off of the Voltage divider rule.                                                           
32/10=1+22k/10k  this is just a calculation of gain for the output buffer. Based on the 22k resistor and the 10K resistor.


## 11/10/2021
Went over old PCB design with Madhav and checked that all components we have will fit on the board old PCB board so that we dont make mistakes on my new PCB design. The LM358 was too small on the board for the physical type we had and the power connector did not match the part we have.
## 11/11/2021
Thoroughly checked over the pcb design and asked questions to David about some of my components getting 0V instead of GND. 
I removed the power jack from the board and added soldering pads because we wanted the flexibility of inputting the power the board at the exact location we want with our pedal chasis.
## 11/15/2021
Reviewed the second pcb design that Madhav made as a backup in case my design doesnt work and found that it was missing MIDI and the combinational logic.
## 11/16/2021
Check the wires names of the schematic and pcb that was ordered. Met in the lab with Madhav to test the new DAC board on teensy.
## 11/17/2021
PCB arrived.
## 11/18/2021
There is an issue with the power system when connected with the teensy. On the breadboard and PCB when powered with 9V the terminal voltage drops to roughly 6 volts after connecting power and GND to the teensy. It consistently drops the voltage by 3V unless the power input to the voltage regulator is 6V or below. A fit on the breadboard was putting a capacitor from the power supply to the volatge regulator to limit the voltage to under 6V. However this cannot be simply done on the PCB we already have.
## 11/19/2021
Built a secound pcb to take home over break.
I tried to get the combinational logic to work with varying resistor values. The best outcome seemed to be hearing the guitar note and a backround of the startup noise of the synth signal but both signals were not combining properly.
Ordered a teensy and arduino to work from home.
## 11/24/2021
Got additional equipment to work on project before the demo.
Uploaded the new code Madhav had into my Teensy.
Figured out solution for the 3V drop across the power supply. It was to clip the input pin of the voltage regulator so teat it was no longer attched to the pcb board and solder a wire from the 9V souldering pad to the clipped input pin of the voltage regulator. This way the trace routes made along the input pin of the voltage regulator could not could not split the 9v power.
## 11/25/2021
Was further testing but getting strange results only noise was coming from the teensy.
## 11/26/2021
Researched the combinational logic to ensure that it should work with our design. Had to fix my at home osclloscope due to buttons being stuck by opening it up. Tested the signal at each phase of the design and noticed an issue with the output of the teensy. I called Madhav and told him how to check it using the oscilloscopes at the lab which gave very different results. My teensy seemed to be operational, but Madhav was using the second pcb design we ordered as a backup in case anything was wrong on the final pcb design.
## 11/27/2021
Built combinational logic or breadboard for further testing since output of the Teensy seemed to only give noise.
## 11/28/2021
Connected combinational logic with a waveform generator.
Connected input buffer logic with a waveform generator to test teensy output.
Printed labels for the exterior of the projects switches and power input. (this never made it to final design because of failure before demo)
I bought an addition teensy for security of the project.
## 11/29/2021
Madhav and I built an additional input buffer for the DAC chip that was needed since ADC now happened on the teensy as opposed to the audio board.
![input buffer](notebook/Danielle/newinput.png "final input buffer")
Reference:
"Audio System Design Tool for Teensy Audio Library" 2021, Available at: https://www.pjrc.com/Teensy/gui/ 
Switches were added and we recorded the complete functionality of the project. Everything worked with combination done in software and with the DAC board instead of the audio board that we added for complexity. We did not complete MIDI since the MIDI cable we would need for verification never arrived on time. 
Madhav said sound had less noise with the auido board but we not to a point where the output was pretty good and we did not want to break anything so we didnt try to change any more circuitry. 
Competed left over verifications and collected osclloscope readings at each point of the project.
![output](notebook/Danielle/Output_buffer.PNG "output buffer verification")
The output buffer correctly amplifies the signal to be 3 times the peak to peak voltage.
![input](notebook/Danielle/input_buffer.PNG "input buffer verification")
The input buffer correctly divieds the signal to be a third of the peak to peak voltage.
![latency](notebook/Danielle/latency.PNG "latency verification")
The latency is only 5ms which is much better than our 10ms limit.
I also measured the impedance or the output buffer and input buffer. We want the input buffer to have a high impedance and the output buffer to have a low impedance. The current is very low because the guitar signal input can only be 500mv maximum. 
I brought a Temperature sensor from home along with a voltmeter that it connects too of the power system is 30 degrees celsius.
## 11/30/2021
Picked up super glue and insulated the teensy to prevent pcb from touching the metal.
A loose connection cause performance to vary based off how the teensy was moved.
During insulation a ground wire came loose and I connected to the AGND which I believe caused overheating and the DAC chip to break.
Ordered a new Teensy and Audio board since ours overheated in final demo. (delivery never came in time)
## 12/01/2021-12/02/2021
Worked on presentation for Mock demo.
## 12/03/2021-12/06/2021
Worked on Final presentation.
## 12/06/2021
Finished up work on presentation and practiced speaking. Started final paper.
## 12/07/2021
Had final presentation at 8:30 am, came in early to reherse with Madhav. Worked on conclusion of final paper.
Made and submitted extra credit video.

## 12/08/2021
Worked all day on on final paper.
My parts
- 1.1 copied over from design doc
- 1.2 my subsystems
- 1.3 Requirements
- 2.2 Block diagram
- 2.3 Design Hardware section
- 3 Design verification 
- 4 Costs 
- 5 Conclusion section 
- Apendix A - hardware subsytem 
- Appendix B - Schedule 
- References 
## 12/09/2021
Lab check out & teamwork form 2.



